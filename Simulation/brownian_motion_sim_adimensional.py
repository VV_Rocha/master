import numpy as np
import matplotlib.pyplot

from numba import jit
from numba.pycc import CC
cc = CC('brownian_motion_sim')

import time

@jit(nopython=True)
def init_parameters(delta_x, delta_v, initial_type):  # generation of random initial parameters
    if initial_type==0:
        x0 = np.zeros(6)  # [x, vx, y, vy, z, vz]
        for i in range(0,6,2):
            x0[i] = delta_x*(1.-2.*np.random.rand())
            x0[i+1] = delta_v*(1.-2.*np.random.rand())
        return x0
    # override the random initial conditions and use the deltass as the initial values
    else:
        return np.array([delta_x[0], delta_v[0], delta_x[1], delta_v[1], delta_x[2], delta_v[2]])
    
@jit(nopython=True)
def force(r, k, naxis, theta):  # w is the parameter waste
    F = np.zeros(naxis)
    if (naxis == 3):
        x, y, z = r[0], r[1], r[2]
        kx, ky, kz = k[0], k[1], k[2]
        F[0] = -1.* (kx*(x*np.cos(theta)-y*np.sin(theta))*np.cos(theta)+(ky*(x*np.sin(theta)+y*np.cos(theta))*np.sin(theta)))
        F[1] = -1.* (-1.*kx*(x*np.cos(theta)-y*np.sin(theta))*np.sin(theta)+(ky*(x*np.sin(theta)+y*np.cos(theta))*np.cos(theta)))
        F[2] = -1.*kz*z
    elif (naxis==1):
        x = r[0]
        kx = k[0]
        F = np.array([-kx*x])
    return F

@jit(nopython=True)
def func(y, fm, diff, fp, delta_t, random, naxis, acquisition_rate, theta, vm, force = force):
    r = y[::2]
    v = y[1::2]

    K = np.zeros(int(2*naxis))
    K[::2] = v
    #print(">>>", acquisition_rate)
    v_storage = np.array([-(acquisition_rate**2)*fp[axis] *(r[axis]+v[axis]*delta_t) - acquisition_rate*fm[axis]*v[axis] + random[axis]/np.sqrt(delta_t) for axis in range(naxis)])
    #v_storage = np.array([-fm[axis]*v[axis] - fp[axis]*(r[axis]+v[axis]*delta_t) + fm[axis]*np.sqrt(2*diff[axis])*random[axis]/np.sqrt(delta_t) for axis in range(naxis)])
    K[1::2] = v_storage
    return K

@jit(nopython=True)
def RK4(y, delta_t, fm, diff, fp, random, naxis, acquisition_rate, theta, func = func, force = force):  # returns the parameter vector increments 
    
    K0 = delta_t *func(y, fm, diff, fp, delta_t/2, random[:,0], naxis, acquisition_rate, theta, np.array([0. for axis in range(naxis)]), force)
    K1 = delta_t *func(y + K0/2., fm, diff, fp, delta_t/2, random[:,1], naxis, acquisition_rate, theta, y[1::2] + K0[1::2]/2, force)
    K2 = delta_t *func(y + K1/2., fm, diff, fp, delta_t/2, random[:,1], naxis, acquisition_rate, theta, y[1::2] + K1[1::2]/2, force)
    K3 = delta_t *func(y + K2, fm, diff, fp, delta_t/2, random[:,2], naxis, acquisition_rate, theta, y[1::2] + K2[1::2], force)

    return (K0 + 2.*K1 + 2.*K2 + K3)/6.
    

@jit(nopython=True)
def run(T, delta_t, fm, diff, fp, naxis, acquisition_rate, theta, initial_parameters, RK4 = RK4, func = func, force = force):  # function that does an simulation run by calling the RK4 integrator T times
    y = np.zeros((int(2*naxis), T))

    rand = np.zeros((naxis,3))

    # setting initial parameters
    y[:, 0] = initial_parameters

    times = np.zeros(int((T-1)))
    for t in range(1,T):
        
        for axis in range(naxis):
            for s in range(1,3):
                rand[axis, s] = np.random.normal(0.,1.)
        
        y[:,t] = y[:,t-1] + RK4(y[:,t-1], delta_t, fm, diff, fp, rand, naxis, acquisition_rate, theta)

        # records the K4 of the current time as K1 of the posterior time. Since the stochastic term is evaluated in time and K4 is evaluated in time t+tau this tame is the same as K0(t+tau)
        rand[:, 0] = rand[:, 2]

    print(">>> Each time step mean elapsed time =", np.mean(times))

    return y

#@jit()
def main(fp, fm, diff, N, T, delta_t, acquisition_rate, initial_parameters=None, naxis = 3, theta = 0., run = run, RK4 = RK4, func = func, force = force):

    #delta_t_adimensional = delta_t/acquisition_rate
    delta_t_adimensional = delta_t

    initial_parameters = np.zeros(int(2*naxis))

    experiments = np.zeros((int(2*naxis), N, T))
    for r in range(N):
        experiments[:,r] = run(T, delta_t_adimensional, fm, diff, fp, naxis, acquisition_rate, theta, initial_parameters)
    print(experiments.shape)
    
    experiments = np.array([experiments[2*axis]*np.sqrt(2.*diff[axis])*(acquisition_rate**(3/2))*fm[axis] for axis in range(naxis)])

    return experiments





















    