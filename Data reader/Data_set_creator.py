import numpy as np
import os
from scipy import *
import pandas as pd
import pickle
import random
from numpy import random
from math import floor
from numba import jit

# Loads ".dat" data files from the data_dir directory returning it and the labels.
# the parameter remove allows the removel of chosen labels data.
def load_timeseries(data_dir, npoints, remove, data_type):  
    path_files_TD = [f for f in os.listdir(data_dir) if f.endswith('.dat')]  # search forlder for data files
        
    # sort the datasets by order of the particles
    path_files_TD=sorted(path_files_TD)
        
    #store the info for each file
    x_windows = [] #xdata  
    y_windows = [] #ydata
    zsum_windows = [] #intensity data
    
    nsegments_min = []
    
    for file in range(len(path_files_TD)):
        current_dir = "".join((data_dir,path_files_TD[file]))
        
        current_df = pd.read_csv(current_dir,skiprows=2,header=None,delimiter='\t')
        
        # record the minimum timeseries length to cut the data
        if file == 0:
            minimum_length = len(current_df[0])
        elif minimum_length > len(current_df[0]):
            minimum_length = len(current_df[0])
        
        # [files, segments, timeseries]
        x_windows.append(current_df[0])
        y_windows.append(current_df[1])
        zsum_windows.append(current_df[2])        
        
    # free memory
    #del current_df
    
    
    nsegments = int(minimum_length/npoints)
    data = np.zeros((3, len(path_files_TD), nsegments, npoints))

    # Mean centering of the position points referencing to the mean position value of each segment
    for p in range(len(path_files_TD)):
        for segment in range(nsegments):
            data[0,p,segment] = x_windows[p][segment*npoints:(segment+1)*npoints] - np.mean(x_windows[p][segment*npoints:(segment+1)*npoints])
            data[1,p,segment] = y_windows[p][segment*npoints:(segment+1)*npoints] - np.mean(y_windows[p][segment*npoints:(segment+1)*npoints])
            data[2,p,segment] = zsum_windows[p][segment*npoints:(segment+1)*npoints] - np.mean(zsum_windows[p][segment*npoints:(segment+1)*npoints])
    
    # free memory
    del x_windows
    del y_windows
    del zsum_windows
    
    targets = np.array([path_files_TD[i].replace(".dat","")[:-1] for i in range(len(path_files_TD))]) #type of target
    #print(targets)
    # loops to remove particles from the set. this way we can limit the study to the remaining particles
    for to_remove in remove:
        index = np.where(targets==to_remove)
        data = np.delete(data[:,:,:], index, axis = 1)
        targets = np.delete(targets, index, axis = 0)
    print(data.shape)
    if data_type==1:
        data = data[:,:,:,1:]-data[:,:,:,:-1]

    return targets, data


# given a set of timeseries returns the 2D histogram plots between the positions "x" and "-x" and "y" and "-y"
def make_image(xdata, ydata, P, x, y):  # data format [image, timeseries]. P is the number of bins
    # creates array of zeros to store the images
    images = np.zeros((len(xdata),P,P))

    xmin, xmax = -x,x
    xedges = np.linspace(xmin, xmax, P+1)
    ymin, ymax = -y,y
    yedges = np.linspace(ymin, ymax, P+1)

    for p in range(len(xdata)):
        hist, xbins, ybins = np.histogram2d(xdata[p],ydata[p], bins=(xedges, yedges), density=False)
        #hist, xbins, ybins = np.histogram2d(xdata[p],ydata[p], range=[[xmin, xmax], [ymin, ymax]], density=False)
        images[p] = hist
    return images

"""
# dictionary type class
class mydatabase:
    def __init__(self, targets, data):
        self.targets = targets
        self.data = data
        self.classes = np.unique(targets, return_counts=False)

    # this code goes through the targets labels and separates the type of particle and size. It is useful if we want to create classes that take into account only size or type.
    def properties(self, keywords):
        def name_filter(name, keywords):
            # removes the micrometer um
            name = name.replace("um","")    #print(name)
            sizes = ""
            names = ""
            for i in name:
                if i.isdigit():
                    sizes += i
            if name == "water":
                sizes += "water"
            #print(name)
            for j in keywords:
                if j in name:
                    names += j
                    continue
            return [names, sizes]

        single_unit = np.array([name_filter(self.targets[i], keywords) for i in range(len(self.targets))])
        properties = {}
        properties["type"] = np.array(single_unit[:,0])
        properties["size"] = np.array(single_unit[:,1])
        return properties
"""

#  normalizes each images max to one [0,1]
def unity_norm(images):
    for classe in range(images.shape[0]):
        for subelements in range(images.shape[1]):
            images[classe, subelements] = images[classe, subelements]/np.max(images[classe, subelements])
    return images 

# data_type=0 for position and 1 for displacement
def image_plotter(data,P=100, nplanes=1, xlim=0.1, ylim=0.1, data_type=0, make_image=make_image, image_normalizer=unity_norm):
    # plotting density histograms without normalization (each bin is the counting of times the particle is found in it, this can be relevant if any normalization is done taking into account different directions)
    images = np.zeros((data.shape[1], data.shape[2], P, P, nplanes))  # [particles, segments, height, width, channels]
    #images_zoom = np.zeros((nplanes, data.shape[1], data.shape[2], P, P))
    channel_indices = np.array([[0,1], [0,2], [1,2]])

    for channel in range(nplanes):
        for classe in range(data.shape[1]):
            images[classe, :, :, :, channel] = make_image(data[channel_indices[channel,0],classe], data[channel_indices[channel,1],classe], P, xlim, ylim)

    # calls function to normalize each images maximum to one
    images = image_normalizer(images)

    return images

"""
def main(npoints = 50000, data_dir="../Data/Experimental data/Synthetic Particles (IAC, 21)/", remove=[], data_type=0, load_data=load_timeseries, P=100, nplanes=1, xlim=0.1, ylim=0.1, make_image=make_image, image_normalizer=unity_norm, filename="dataset", image_plotter=image_plotter)
    # read and extract position timeseries onto arrays of segments for each particle. The positions are mean centered
    targets, data = load_data(npoints, data_dir, remove, data_type=data_type)

    images = image_plotter(data, P=100, nplanes=1, xlim=0.1, ylim=0.1, make_image=make_image, image_normalizer=unity_norm, mydatabase=mydatabase, filename="dataset")

    return images, targets
"""