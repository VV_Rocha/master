import numpy as np
import keras
import pickle

from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Activation, Dense
from keras.layers import Dropout
from keras.utils import np_utils
from keras.layers import Flatten, GaussianNoise
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import MaxPooling2D, AveragePooling2D
from keras.utils.vis_utils import plot_model
from keras.optimizers import Adam, SGD
from keras.models import load_model
from keras import layers
from keras import regularizers

from matplotlib.pyplot import *

def one_hot_encoding(targets):
    return np.array([np.where(np.unique(targets)==targets[i])[0][0] for i in range(targets.shape[0])])


# This function runs the model training and returns the desired quantities
def train_model(model, X_train, y_train, batch_size=32, epochs=20, X_val=[], y_val=[], validation_split=0., verbose=2, shuffle=True):
    history = model.fit(X_train, y_train,
                        validation_data=(X_val, y_val),
                        epochs=epochs, shuffle=shuffle, batch_size=batch_size, verbose=verbose)

    return model, history.history["accuracy"], history.history["loss"], history.history["val_accuracy"], history.history["val_loss"]

# performs the k-fold split returning indices of the data for the test
def fold_split(k, targets):
    # randomizes the order of the indices inside each class. In each line resides the indices of a class
    index_matrix = np.array([np.random.permutation(np.where(np.unique(targets)[i] == targets)[0]) for i in range(len(np.unique(targets)))])
    # choose the k sets of columns of randomized indices with floor(len(data)/k) items.
    #a = np.array([np.reshape(index_matrix[:,i*k:(i+1)*k], (len(np.unique(targets))*k)) for i in range(int(np.floor((index_matrix.shape[1])/k)))])

    n = np.floor(index_matrix.shape[1]/k).astype("int")  # number of points from each class inside a fold

    a = index_matrix[:,:k*n].T.reshape((k, len(np.unique(targets))*n))  # chooses the n first columns of index_matrix
    return a


# performs a k-fold cross-validation
def k_fold(X, y, k, model_parameters, model, batch=32, epochs=20, X_val=[], y_val=[], validation_split=0., verbose=2, shuffle=True, combination=fold_split, train=train_model):
    """
    X must have shape (targets, segments, *datashape)
    y must have shape (targets, )
    """

    combinations = fold_split(k, y)
    print(combinations)
    ### Metric storing bins:
    accuracy = np.zeros((k, epochs))
    loss = np.zeros((k, epochs))
    val_accuracy = np.zeros((k, epochs))
    val_loss = np.zeros((k, epochs))
    likelihoods = np.zeros((k, int(len(y)/combinations.shape[0])*X.shape[1], len(np.unique(y))))

    # cycle through folds training untrained models using different training/validation combinations
    for fold in range(k):
        print(">>> " + "fold " + str(fold+1) +"/"+str(k))

        trained_model = model(*model_parameters)

        trained_model, accuracy[fold], loss[fold], val_accuracy[fold], val_loss[fold] = train(trained_model, X_train=np.delete(X, combinations[fold],axis=0).reshape(((X.shape[0]-len(combinations[fold]))*X.shape[1], *X.shape[2:])),
                                                                                            y_train=np_utils.to_categorical(np.delete(y,combinations[fold],axis=0).repeat(X.shape[1])), batch_size=batch,
                                                                                            epochs=epochs, X_val=X[combinations[fold]].reshape((int(np.floor(len(y)/k)*X.shape[1]), *X.shape[2:])),
                                                                                            y_val=np_utils.to_categorical(y[combinations[fold]].repeat(X.shape[1])), validation_split=validation_split,
                                                                                            verbose=verbose, shuffle=shuffle)

        likelihoods[fold] = trained_model.predict(X[combinations[fold]].reshape((int(np.floor(len(y)/k)*X.shape[1]), *X.shape[2:])), verbose=0)

    return accuracy, loss, val_accuracy, val_loss, likelihoods, combinations

def deep_network_model(input_shape=(64, 64, 3), nclass=2, lr=10**(-3), l1=None, l2=None):
    model = Sequential()

    model.add(Conv2D(1, (3,3), strides=1, input_shape=input_shape, activation='relu', kernel_initializer="he_uniform"))
    model.add(MaxPooling2D((2,2), strides=2))

    model.add(Flatten())

    model.add(Dense(1024, activation='relu'))
    #model.add(Dropout(1.))

    model.add(Dense(512, activation='relu'))
    #model.add(Dropout(1.))

    model.add(Dense(256, activation='relu'))
    #model.add(Dropout(1.))

    model.add(Dense(128, activation='relu'))
    #model.add(Dropout(1.))

    model.add(Dense(64, activation='relu'))
    #model.add(Dropout(1.))

    activation='softmax'
    model.add(Dense(nclass, activation=activation))

    # Compile model
    opt = Adam(lr=lr)
    #opt = SGD(learning_rate=0.001, momentum=0.0, nesterov=False)
    loss = "categorical_crossentropy"
    model.compile(optimizer=opt, loss=loss, metrics=["accuracy"])
    return model

# performs the cycle over several k-fold cross-validations
def markov_cycle(X, y, k, M, model_parameters=((64, 64, 3), 2, 10**(-3), None, None), model=deep_network_model, batch=32, epochs=20, X_val=[], y_val=[], validation_split=0., verbose=2, shuffle=True, combination=fold_split, fit=train_model, cross_validation=k_fold):
    """
    X must have shape (targets, segments, *datashape)
    y must have shape (targets, )
    """

    ### Metric storing bins:
    accuracy = np.zeros((M, k, epochs))
    loss = np.zeros((M, k, epochs))
    val_accuracy = np.zeros((M, k, epochs))
    val_loss = np.zeros((M, k, epochs))

    combinations = np.zeros((M, k, int(np.floor(y.shape[0]/k))))
    likelihoods = np.zeros((M, k, int(len(y)/k)*X.shape[1], len(np.unique(y))))

    for m in range(M):
        print(">>> " + "Markov step " + str(m+1) + "/"+str(M))
        accuracy[m], loss[m], val_accuracy[m], val_loss[m], likelihoods[m], combinations[m] = cross_validation(X, y, k, model_parameters, model, batch=batch, epochs=epochs, X_val=[], y_val=[], validation_split=0., verbose=2, shuffle=True, combination=fold_split, train=train_model)

    return accuracy, loss, val_accuracy, val_loss, likelihoods, combinations

class results:
    def __init__(self, classes, targets, nsegments, accuracy, loss, val_accuracy, val_loss, likelihoods,
            combinations, M, k, epochs, bins, window, npoints, dt):

        self.classes = classes
        self.targets = targets
        self.nsegments = nsegments
        self.accuracy = accuracy
        self.loss = loss
        self.val_accuracy = val_accuracy
        self.val_loss = val_loss
        self.likelihoods = likelihoods
        self.combinations = combinations
        self.M = M
        self.k = k
        self.epochs = epochs
        self.bins = bins
        self.window = window
        self.npoints = npoints
        self.dt = dt

    def likelihood_to_predictions(self):
        predictions = np.array([[[self.classes[np.where(self.likelihoods[i,j,k]==np.max(self.likelihoods[i,j,k]))[0]] for k in range(self.likelihoods.shape[2])] for j in range(self.likelihoods.shape[1])] for i in range(self.likelihoods.shape[0])])
        return predictions.reshape((predictions.shape[0],predictions.shape[1],predictions.shape[2]))

    def confusion_matrix(self):
        cm = np.zeros((len(self.classes),len(self.classes)))
        for c1 in range(len(self.classes)):
            for c2 in range(len(self.classes)):
                cm[c1,c2] = len(np.intersect1d(np.where(self.targets[self.combinations.repeat(self.nsegments,axis=2).reshape(-1)]==self.classes[c1])[0], np.where(self.likelihood_to_predictions().reshape(-1)==self.classes[c2])[0]))
        return cm

    def plot_confusion_matrix(self, cm):
        import seaborn as sns
        import matplotlib.pyplot as plt

        #cmap=cm.Blues
        cm_norm = np.zeros(cm.shape)
        for i in range(cm.shape[0]):
            cm_norm[i] = cm[i]/np.sum(cm[i])

        fig, axs = subplots(1)
        im=axs.imshow(cm_norm,interpolation='nearest',cmap="Blues")
        #im.clim(0, 1)
        fig.colorbar(im)

        tick_marks=np.arange(len(self.classes))
        #axs.set_xticks(tick_marks,self.classes,rotation=45,fontsize=12)
        #axs.set_yticks(tick_marks,self.classes,rotation=0,fontsize=12)

        thresh = 0.5
        for i in range(cm.shape[0]):
            percentage = [round(cm[i,k]/ np.sum(cm[i,:]),2) for k in range(cm.shape[1])]
            for j in range(cm.shape[1]):
                axs.text(j,i, percentage[j], horizontalalignment="center",fontsize=14, color="white" if percentage[j] > thresh else "black")

                axs.text(j, i+0.2, "(" + str(int(cm[i,j])) + ")", horizontalalignment="center",fontsize=12, color="white" if percentage[j] > thresh else "black")


        axs.set_xticks(np.arange(len(self.classes))-0.5, minor=True)
        axs.set_yticks(np.arange(len(self.classes))-0.5, minor=True)
        grid(which="minor", color='k', linestyle='-', linewidth=1)

        tight_layout()

        ylabel('True label',fontsize=14)
        xlabel('Predicted label',fontsize=14)


empty_message = "The metric you request appears to be empty!"
def save_results(save_dir, classes=empty_message, targets=empty_message, nsegments=empty_message, accuracy=empty_message, loss=empty_message, val_accuracy=empty_message,
                val_loss=empty_message, likelihoods=empty_message, combinations=empty_message,M=empty_message,
                k=empty_message, epochs=empty_message, bins=empty_message, window=empty_message, npoints=empty_message, dt=empty_message):
    with open(save_dir + '.pickle', 'wb') as handle:
        pickle.dump(results(classes=classes, targets=targets, nsegments=nsegments, accuracy=accuracy, loss=loss, val_accuracy=val_accuracy, val_loss=val_loss, likelihoods=likelihoods, combinations=combinations, M=M, k=k, epochs=epochs, bins=bins, window=window, npoints=npoints, dt=dt), handle)


def load_results(save_dir):
    with open(save_dir + '.pickle', 'rb') as handle:
        b = pickle.load(handle)
    return b




