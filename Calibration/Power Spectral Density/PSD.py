import numpy as np
import scipy as sp
from scipy import signal
from scipy.optimize import curve_fit

# Return the welchs result corrected with the factor of 2
def PSD(data, dt, nperseg):
    freqs, psd = sp.signal.welch(data, 1/dt, nperseg=nperseg)
    return freqs, psd/2.

# Exact power spectral density as mass is typically very small it is better to use the inertia solution.
def Lorentzian(f, k, gamma, D, m):
    return gamma**2 * D/(np.pi**2 * ((2*np.pi*m)**2 * (-f**2 + k/((2*np.pi)**2 *m))**2 + gamma**2 *f**2))

# returns the log of the power spectral density for small reynolds number
def Lorentzian(freq, D, cut_frequency):
    return D / ((2*np.pi**2) * (cut_frequency**2 + freq**2))

#
def main(data, dt, nperseg, naxis, mean=True, PSD=PSD, Lorentzian=Lorentzian):
    psd_storage = np.zeros((naxis, int(nperseg/2 +1)))  # Store power spectral density values
    # Store frequencies, since time steps and acquisition times are the same for all coordinates
    # we need only store for one direction.
    freqs = np.zeros(int(nperseg/2 +1))
    for axis in range(naxis):
        freqs, psd_storage[axis, :] = PSD(data[axis, :], dt, nperseg)
    # cropping bellow the Nyquist freq to avoid aliasing
    psd_storage = psd_storage[:,np.where(freqs<=1./(2.*dt))]
    psd_storage = psd_storage[:,0,:]
    freqs = freqs[np.where(freqs<=1./(2.*dt))]

    parameter = np.zeros((data.shape[0], 2))
    parameter_variance = np.zeros((data.shape[0],2,2))
    for axis in range(data.shape[0]):
        parameter[axis], parameter_variance[axis] = sp.optimize.curve_fit(Lorentzian, freqs, psd_storage[axis], bounds=([0., 0.],[np.inf, np.inf]))
    parameter_variance = ([parameter_variance[:,i,i] for i in range(parameter_variance.shape[1])])
    return psd_storage, freqs, parameter, parameter_variance
