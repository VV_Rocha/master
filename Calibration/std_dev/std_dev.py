import numpy as np

def main(data, gama, diff):
    """
    main function returns the stiffness, equilibrium position, and stiffness variation
    accordingly to Boltzmann statistics
    Parameters: data - array of [cartesian directions, position timeseries]
                gama - viscosity coefficient
                diff - diffusion coefficient
    Note: For experimental datasets, one tipically has the systems temperature where
    it might be usefull to use Einstein(consultar o nome da relação) relation
                    K_B T = diff * gama,
    where K_B is the Boltzmann constant and T the systems temperature. For this set
    gama = K_B * T_experimental and diff=1.
    """
    var = np.full((data.shape[0]), 0.)
    mean = np.full((data.shape[0]), 0.)
    for axis in range(data.shape[0]):
        var[axis] = np.var(data[axis], ddof=0.)
        mean[axis] = np.mean(data[axis])
    x_eq = np.zeros(data.shape[0])
    for axis in range(data.shape[0]):
        x_eq[axis] = np.mean(mean[axis])

    k_estimate = np.array([np.mean(gama*diff[axis]/var[axis]) for axis in range(data.shape[0])])
    k_var = np.array([np.mean(gama*diff[axis]/k_estimate[axis]) for axis in range(data.shape[0])])

    return k_estimate, k_var, x_eq
