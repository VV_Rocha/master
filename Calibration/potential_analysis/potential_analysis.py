import numpy as np
import scipy as sp
from scipy.optimize import curve_fit

def main(data, n_bins, gama, diff):
    """
    main function estimates the potential well shape, Hook stiffness in a harmonic potential
    well approximation, and the position equilibrium position.

    Parameter: data - array of position timeseries. Shape [cartesian axis, position timeseries]
               n_bins - number of bins used in the histograms and consequently for precision and
    resolution of potential well estimate
               gama - viscosity coefficient
               diff - diffusion coefficient

    Note: For experimental datasets, one tipically has the systems temperature where
    it might be usefull to use Einstein(consultar o nome da relação) relation
                     K_B T = diff * gama,
    where K_B is the Boltzmann constant and T the systems temperature. For this set
    gama = K_B * T_experimental and diff=1.
    """
    hist = np.zeros((data.shape[0], n_bins))  # Store frequency
    bins = np.zeros((data.shape[0], n_bins+1))  # Store bin values
    # Compute the normalized histogram
    for axis in range(data.shape[0]):
        hist[axis], bins[axis] = np.histogram(data[axis], n_bins, density=True)
    bins = (bins[:,1:]+bins[:,:-1])/2.  # tranform bin edges to bin mid values
    U = np.zeros(hist.shape)  # Store potential estimates
    # For each axis obtain the potential well shape by inverting rho=exp[-U/k_B*T]
    # ignore values where the density is zero, this sets U at that position to zero
    for axis in range(data.shape[0]):
        U[axis,np.where(hist[axis]!=0.)] = -diff[axis]*gama*np.log(hist[axis,np.where(hist[axis]!=0.)])
    # estimate parameters
    parameter_estimate = np.zeros((data.shape[0]))  # Store estimates of stiffness
    parameter_variance = np.zeros((data.shape[0]))  # Store variance of the estimates
    for axis in range(data.shape[0]):
        D = diff[axis]  # defining D this way allows it to be passed to function f without being a parameter
        def f(x, k):  # take the harmonic potential approximation of the energy
            x_eq = 0.
            return np.sqrt(k/(2.*np.pi*gama*D)) * np.exp(-k * (x - x_eq)**2. / (2.*gama*D))
        parameter_estimate[axis], var_matrix = sp.optimize.curve_fit(f, bins[axis], hist[axis], bounds=((0),(np.inf)))
        # parameter variance is the diagonal of the covariance matrix
        #parameter_variance[axis] = np.array([var_matrix[i,i] for i in range(2)])
        parameter_variance = var_matrix
    return U, bins, parameter_estimate
