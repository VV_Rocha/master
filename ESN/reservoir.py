import numpy as np

#from numba import jit
#from numba.pycc import CC

#cc = CC("reservoir")


class Reservoir(object):
    """
    In the end write the entry
    """
    def __init__(self,
                 N_x=100,
                 spectral_radius=.99,
                 leak=None,
                 internal_connectivity=(.1,"ratio"),
                 input_connectivity=(1.,"ratio"),
                 scaling=1.,
                 ):

        # Reservoir attributes
        self.N_x = N_x
        self.internal_connectivity = internal_connectivity
        self.spectral_radius = spectral_radius
        self.input_connectivity = input_connectivity
        self.scaling = scaling
        self.leak = leak

        # Generate internal weight matrix
        self.W = self.sparse_matrix(self.N_x,
                                    self.N_x,
                                    self.internal_connectivity,
                                    self.spectral_radius,
                                    scaling="spectral_radius"
                                    )
        # Initiate
        self.W_in = None

    def default_weight_distribution(size, deviation):
        return np.random.normal(scale=deviation, size=size)

    def sparse_matrix(self,
                      n_rows,
                      n_cols,
                      connectivity,
                      spectral_radius,
                      weights_distribution=default_weight_distribution,
                      kargs=(1.,),
                      scaling="spectral_radius",
                      ):
        """
        Generates a weight matrix with given connectivity.

        Parameteres:
            n_rows = number of rows
            n_cols = number of colummns
            connectivity = tuple of (value, type). Type can be "ratio" or "number of connections".
            spectral_radius = maximal spectral_radius of the weight matrix.
            weights_distribution = function of parameters number of random weights and kargs (deviation,...)
        """
        # Store for matrix
        matrix = np.zeros((n_rows, n_cols))
        # For connectivity "ratio" compute the number of connections.
        # On a first instance the actual ratio might be smaller than the asked value
        # correct this later.
        if connectivity[1] == "ratio":
            n_connections = int(n_cols*connectivity[0])
        elif connectivity[1] == "number of connections":
            n_connections = connectivity[0]
        # Number of connections define the number of nonzero elements in the row.
        for row_index in range(n_rows):
            # Select n_connections random indices
            #indices = np.random.choice(np.arange(n_cols), size=n_connections, replace=False)
            indices = np.random.choice(np.arange(n_cols), size=n_connections, replace=False)
            # Set the indices equal the random values
            matrix[row_index, indices] = weights_distribution(n_connections, *kargs)
        if scaling == "spectral_radius":
            # Correct the spectral radius
            eig, _ = np.linalg.eig(matrix)
            eig_max = np.max(np.abs(eig))
            matrix *= spectral_radius/np.abs(eig_max)
        elif scaling == "scaling":
            matrix *= self.scaling*matrix
        return matrix

    def compute_states_history(self, X, transient_time=0, activation_function=np.tanh):
        """
        Computes the histoy of states for a given input.

        Parameteres:
            X = input data
            transient_time = time before storing the states
        """
        n_samples, _, time_length = X.shape
        # Initial state will be zero vector
        previous_state = np.zeros((n_samples, self.N_x), dtype=float)
        # Storage of states
        states_history = np.zeros((n_samples, self.N_x, time_length-transient_time), dtype=float)
        # Parallel cycle of samples in time
        for t in range(time_length):
            # New state prior to nonlinear transformation
            print(self.W.dot(previous_state.T).shape)
            print(self.W_in.dot(X[:,:,t].T))
            posterior_state = self.W.dot(previous_state.T) + self.W_in.dot(X[:,:,t].T)
            # Apply nonlinearity
            if self.leak is None:
                posterior_state = activation_function(posterior_state)
            else:
                posterior_state = (1.-self.leak)*previous_state + activation_function(posterior_state)

            if t >= time_length:
                states_history[:,:,t-ndrop] = posterior_state
        return states_history

    def get_states(self, X, transient_time=0):
        """
        Generates an input weight matrix if need be and calls the compute_states_history.
        Returns the states history matrix of every s input sample.

        Parameters:
            X = input data
            transient_time = time before starting to store the reservoir states. Used to forget the starting state influence.
        """
        n_samples, dim, time_length = X.shape
        if self.W_in is None:
            self.W_in = self.sparse_matrix(self.N_x, dim, self.input_connectivity, self.scaling, scaling="scaling")
        states_history = self.compute_states_history(X, transient_time=transient_time)
        return states_history

c = Reservoir()



