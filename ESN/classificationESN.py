import numpy

# Custom modules
from reservoir import Reservoir

class ClassificationESN():
    def __init__(self,
                 reservoir=None,
                 N_x=100,
                 spectral_radius=.99,
                 leak=None,
                 internal_connectivity=(.1,"ratio"),
                 input_connectivity=(1.,"ratio"),
                 scaling=1.,
                 transient_time=0,
                 ):
        # Reservoir parameters
        self.N_x = N_x
        self.spectral_radius = spectral_radius
        self.leak = leak
        self.internal_connectivity = internal_connectivity
        self.input_connectivity = input_connectivity
        self.scaling = scaling
        self.transient_time = transient_time

        # Initialize reservoir if need be.
        self.reservoir = Reservoir(N_x,
                                   spectral_radius,
                                   leak,
                                   internal_connectivity,
                                   input_connectivity,
                                   scaling)

    def fit(self, X, y):
        # Compute the states
        states_history = self.reservoir.get_states(X,
                                                       transient_time=self.transient_time)


