import numpy as np

def sinc(x):
    result = np.zeros(x.shape[0])
    result[np.where(x==0.)] = 1.
    result[np.where(x!=0.)] = np.sin(x[np.where(x!=0.)])/x[np.where(x!=0.)]
    return result

def weights(n):
    return np.random.uniform(0.,1., n)

## EXTREME LEARNING MACHINE
def ELM(x, y, n=10, g=sinc, ex_par=[], std=0.1, use_bias=False, weights=weights, w_par=[]):
    # step 1
    w, b = weights(n, w_par), np.random.uniform(0.,1., n)

    w = np.zeros((n))
    for node in range(n):
        w[node] = np.random.normal(2*node*std, std)

    # step 2
    if use_bias==False:
        H = np.array([g(np.array([w[i]*x[j] for i in range(n)]), *ex_par) for j in range(x.shape[0])])
    else:
        H = np.array([g(np.array([w[i]*x[j] + b[i] for i in range(n)]), *ex_par) for j in range(x.shape[0])])
    
    #  pseudo-inverse
    Ht = np.linalg.pinv(H, hermitian=False)
        
    B = np.array([np.sum(Ht[i]*y) for i in range(n)])
    
    return B, H, w, b

def ELM_test(x, w, b, use_bias=False, n=10, g=sinc, ex_par=[]):
    if use_bias==False:
        H = np.array([g(np.array([w[i]*x[j] for i in range(n)]), *ex_par) for j in range(x.shape[0])])
    else:
        H = np.array([g(np.array([w[i]*x[j] + b[i] for i in range(n)]), *ex_par) for j in range(x.shape[0])])
    return H

def prediction(H, B):
    return np.array([np.sum(H[i]*B) for i in range(H.shape[0])])

def loss(y, t):
    return np.sum((y-t)**2)
